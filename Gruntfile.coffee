
module.exports = (grunt) ->
  # Project configuration.
  config = {
    pkg: grunt.file.readJSON('package.json'),
    coffee: {
      compileBare: {
        options: {
          bare: true
        },
        files: {
          'binaryheap.js': 'binaryheap.coffee'
        }
      }
    },
    coffeelint: {
      app: ['*.coffee']
    },
    uglify: {
      build: {
        files: {
          'dist/binaryheap.min.js': 'binaryheap.js'
        }
      }
    },
    jshint: {
      options: {
        laxbreak: true
      },
      all: ['binaryheap.js']
    },
    clean: {
      dist: ["dist", "binaryheap.js"]
    },
    jsonlint: {
      dist: {
        src: [ 'package.json' ]
      }
    }
  }
  
  grunt.initConfig(config)
  
  # Load plugins
  grunt.loadNpmTasks('grunt-contrib-uglify')
  grunt.loadNpmTasks('grunt-contrib-jshint')
  grunt.loadNpmTasks('grunt-contrib-coffee')
  grunt.loadNpmTasks('grunt-contrib-clean')
  grunt.loadNpmTasks('grunt-coffeelint')
  grunt.loadNpmTasks('grunt-jsonlint')
  
  # Tasks
  allTasks = ['clean', 'jsonlint', 'coffee', 'coffeelint', 'uglify', 'jshint']
  grunt.registerTask('all', allTasks)
  
  grunt.registerTask('default', ['all'])
